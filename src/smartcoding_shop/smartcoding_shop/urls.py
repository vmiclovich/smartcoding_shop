from django.contrib import admin
from django.urls import path, include
from inventory.views import index, about
from marketing.views import team

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('cart/', include('cart.urls')),
    path('product/', include('inventory.urls')),
    path('about/', about, name='about'),
    path('team/', team, name='team'),
    path('stripe/', include('djstripe.urls', namespace='djstripe')),
]