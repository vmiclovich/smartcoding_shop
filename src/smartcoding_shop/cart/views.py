from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.http import require_POST
from inventory.models import Product
import math
from payments.models import Payment
from .cart import Cart
from .forms import AddProductToCartForm
import stripe

stripe.api_key = "sk_test_RR2Rq4iUe11rYdOTt6ka06Up"


@require_POST
def add_to_cart(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = AddProductToCartForm(request.POST)
    if form.is_valid():
        cleaned_data = form.cleaned_data
        cart.add(product=product, quantity=cleaned_data['quantity'], update_quantity=cleaned_data['quantity'])
    return redirect('cart:cart_detail')


def remove_from_cart(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart:cart_detail')


def cart_detail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = AddProductToCartForm(initial={'quantity': item['quantity'], 'update': True})
    return render(request, 'cart/detail.html', {'cart': cart})


# TODO -> more work has to be done "after holiday" to properly extra addresses of the customer
def proceed_to_checkout(request):
    cart = Cart(request)
    total = math.ceil(cart.get_total_price()*100)

    if request.method == 'POST':
        stripeToken = request.POST['stripeToken']
        charge = stripe.Charge.create(
            amount=total,
            currency='eur',
            description='Amount charge, TODO -> replace with email of customer',
            source=stripeToken
        )
        payment = Payment(stripe_id=charge.id, amount=cart.get_total_price(), paid=charge.paid)
        payment.save()
        # empty cart
        cart.clear()
        return redirect('cart:confirmed')
    return render(request, 'cart/checkout.html', {'cart': cart, 'total': total})
