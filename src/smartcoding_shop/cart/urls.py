from django.urls import path
from django.views.generic import TemplateView
from . import views

app_name = 'cart'
urlpatterns = [
    path('', views.cart_detail, name='cart_detail'),
    path('add/<int:product_id>/', views.add_to_cart, name='cart_add'),
    path('remove/<int:product_id>/', views.remove_from_cart, name='cart_remove'),
    path('checkout/', views.proceed_to_checkout, name='proceed-to-checkout'),
    path('confirmed/', TemplateView.as_view(template_name='cart/confirmed.html'), name='confirmed'),
    path('failed/', TemplateView.as_view(template_name='cart/failed.html'), name='failed'),

]
