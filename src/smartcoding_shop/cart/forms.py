from django import forms


class AddProductToCartForm(forms.Form):
    quantity = forms.TypedChoiceField(choices=[(i, str(i)) for i in range(1, 10)])
    update = forms.BooleanField(required=False, initial=False, widget=forms.HiddenInput)
