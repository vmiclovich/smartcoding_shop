from django.forms import ModelForm
from cloudinary.forms import CloudinaryJsFileField, CloudinaryUnsignedJsFileField

from cloudinary.compat import to_bytes
import cloudinary, hashlib
from .models import Product

class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = '__all__'

class ProductDirectForm(ProductForm):
    image = CloudinaryJsFileField()

class ProductUnsignedDirectForm(ProductForm):
    upload_preset_name = 'sample_' + hashlib.sha1(to_bytes(cloudinary.config().api_key + cloudinary.config().api_secret)).hexdigest()[0:10]
    image = CloudinaryUnsignedJsFileField(upload_preset_name)
