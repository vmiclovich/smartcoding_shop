from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.urls import reverse
from cloudinary import api
from cloudinary.forms import cl_init_js_callbacks
import six
from .forms import ProductForm, ProductDirectForm, ProductUnsignedDirectForm
from .models import Product
from cart.forms import AddProductToCartForm


def about(request):
    return render(request, 'inventory/about.html')


def index(request):
    return render(request, 'inventory/index.html')


def detail(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    cart_product_form = AddProductToCartForm()
    return render(request, 'inventory/detail.html', dict(product=product, cart_product_form=cart_product_form))


def add_product(request):
    unsigned = request.GET.get('unsigned') == 'true'

    if (unsigned):
        try:
            api.upload_preset(ProductUnsignedDirectForm.upload_preset_name)
        except api.NotFound:
            api.create_upload_preset(name=ProductUnsignedDirectForm.upload_preset_name, unsigned=True,
                                     folder='preset_folder')

    direct_form = ProductUnsignedDirectForm() if unsigned else ProductDirectForm()
    context = dict(backend_form=ProductForm(), direct_form=direct_form, unsigned=unsigned)
    # when using direct upload - the following is necessary to update the form's callback url
    cl_init_js_callbacks(context['direct_form'], request)

    if request.method == 'POST':
        # when using backend upload
        form = ProductForm(request.POST, request.FILES)
        context['posted'] = form.instance
        if form.is_valid():
            # upload image and create model instance
            form.save()
            return HttpResponseRedirect(reverse('inventory:product_detail', args=(form.instance.id,)))

    return render(request, 'inventory/add_product.html', context)


def filter_nones(d):
    return dict((k, v) for k, v in six.iteritems(d) if v is not None)


def product_list(request):
    defaults = dict(format='jpg', height=150, width=150)
    defaults['class'] = 'thumbnail inline'

    # The different transformations to present
    samples = [
        dict(crop="fill", radius=10),
        dict(crop="scale"),
        dict(crop="fit", format="png"),
        dict(crop="thumb", gravity="face"),
        dict(format="png", angle=20, height=None, width=None, transformation=[
            dict(crop="fill", gravity="north", width=150, height=150, effect="sepia"),
        ]),
    ]
    samples = [filter_nones(dict(defaults, **sample)) for sample in samples]
    return render(request, 'inventory/products.html', dict(products=Product.objects.all(), samples=samples))
