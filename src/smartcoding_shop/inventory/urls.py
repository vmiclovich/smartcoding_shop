from django.urls import path
from . import views

app_name = 'inventory'
urlpatterns = [
    path('', views.product_list, name='catalog'),
    path('add-product/', views.add_product, name='add_product'),
    path('<int:product_id>/', views.detail, name='product_detail')
]
