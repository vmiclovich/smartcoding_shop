from django.db import models
from django.conf import settings
import stripe


class Payment(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address_line1 = models.CharField(max_length=100)
    address_line2 = models.CharField(max_length=100, blank=True)
    stripe_id = models.CharField(max_length=30, blank=True)

    amount = models.DecimalField(decimal_places=2, max_digits=10)
    # assumed currency is EUR
    currency = models.CharField(max_length=5)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    paid = models.BooleanField(default=False)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    def charge(self, request, email, amount):
        stripe.api_key = settings.STRIPE_TEST_SECRET_KEY

        token = request.POST['stripeToken']

        # customer create
        customer = stripe.Customer.create(card=token, desription=email)
        self.stripe_id = customer.id

        self.save()

        stripe.Charge.create(amount=amount, currence='eur', customer=customer.id)

        return customer
