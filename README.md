# Shop

Practice application for learning Django.

## Dependencies of the project

- MySQL
- Django 2.1
- Python 3

Other tools used:

- Cloudinary
- Stripe

You may also use a virtual environment

[https://docs.python.org/3/library/venv.html](https://docs.python.org/3/library/venv.html)

### Creating a Virtual Env

***Instructions are for UNIX e.g. Mac and Linux*** 

We can call our virtual environment, `venv` too.

`python -m venv venv`

Next step is to activate the virtual environment

`source venv/bin/activate`

To deactivate an environment, you simply type `deactivate` into your terminal.

### Installing Python dependencies

`pip install -r requirements.txt` 

A special file called `requirements.txt` has been provided to help.

### Setting up docker

`docker-compose build` (if this is the first time)

Startup all your other docker images.

`docker-compose up`

### Updating your System environment variables

A few copies have been provided such as `.env.copy`.

Make a copy of that file and call it `.env`

After that replace it with values for Cloudinary's special [https://cloudinary.com/console](console), copy and paste your Environment variable into the `.env` file (delete what you have as placeholders).